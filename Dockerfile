FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

ENV SRS_DOMAIN sutty.nl
ENV SRS_SECRET /etc/postsrsd.secret

RUN apk add --no-cache postsrsd
COPY ./monit.conf /etc/monit.d/postsrsd.conf
COPY ./postsrsd.sh /usr/local/bin/postsrsd

EXPOSE 10001
EXPOSE 10002
