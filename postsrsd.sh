#!/bin/sh
set -e

export SRS_PID_FILE=/tmp/postsrsd.pid

case "$1" in
  stop)
    test -f "${SRS_PID_FILE}" && cat /tmp/postsrsd.pid | xargs -r kill
    exit $?
    ;;
esac

test -n "${SRS_DOMAIN}"
test -n "${SRS_SECRET}"
test -f "${SRS_SECRET}" || dd if=/dev/urandom bs=18 count=1 status=none | base64 > "${SRS_SECRET}"

/usr/sbin/postsrsd -e -D -l 0.0.0.0 -u postsrsd -4
